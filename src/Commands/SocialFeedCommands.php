<?php

namespace Drupal\social_network_feed\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class SocialFeedCommands extends DrushCommands {

  /**
   * Updates cache for each social media account.
   *
   * @usage social_network_feed:update_social_feeds
   *   Usage description
   *
   * @command social_network_feed:update_social_feeds
   * @aliases update-social-feeds
   */
  function update_social_feeds() {
    $database = \Drupal::database();
    // Get all account handles.
    $query = $database->select('paragraph_revision__field_account_handle', 'ahr')
      ->fields('ahr', ['field_account_handle_value' => 'field_account_handle_value']);
    $results = $query->distinct()->execute()->fetchAll();

    if (empty($results)) {
      return;
    }
    $networks = ['fb', 'ig', 'li', 'tw', 'yt'];
    $active_handles = [];

    // Check account handles are still in config forms.
    foreach ($networks as $network) {
      $account_config = \Drupal::config('social_network_feed.config')->get($network . '_accounts');
      if (isset($account_config)) {
        foreach ($account_config as $account) {
          $active_handles[] = $network . '_' . $account[$network . '_handle'];
        }
      }
    }

    foreach ($results as $key => $result) {
      $account = $result->field_account_handle_value;
      // If handle is not in form, then don't get it's feed.
      if (!in_array($account, $active_handles)) {
        unset($results[$key]);
        continue;
      }

      // Delete cache to insure the services create a new cache.
      if (\Drupal::cache()->get('social_network_feed:' . $account)) {
        \Drupal::cache()->delete('social_network_feed:' . $account);
      }

      $input = explode('_', $account, 2);
      $platform = $input[0];
      $handle = $input[1];

      switch ($platform) {
        case 'fb':
          $set_cache = \Drupal::service('social_network_feed.facebook_feed')->getData($handle);
          break;

        case 'ig':
          $set_cache = \Drupal::service('social_network_feed.instagram_feed')->getData($handle);
          break;

        case 'li':
          break;

        case 'tw':
          $set_cache = \Drupal::service('social_network_feed.twitter_feed')->getData($handle);
          break;

        case 'yt':
          $set_cache = \Drupal::service('social_network_feed.youtube_feed')->getData($handle);
          break;
      }
    }
  }
}
