<?php

/**
 * @file
 * Contains \Drupal\social_network_feed\InstagramFeed.
 */

namespace Drupal\social_network_feed;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Uses Instagram Basic Display API to retrieve profile posts.
 */
class InstagramFeed {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Cache backend instance to use.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The posts to be retrieved and returned by the service.
   *
   * @var array
   */
  protected $posts = [];

  /**
   * Constructs a new InstagramFeed object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(ConfigFactoryInterface $config, CacheBackendInterface $cache) {
    $this->config = $config->getEditable('social_network_feed.config');
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.config')
    );
  }

  /**
   * Checks the feed in cache and if not calls getIGPosts().
   *
   * @param $handle
   *   The account handle to retrieve posts from.
   *
   * @return array
   *   The renderable array with retrieved posts.
   */
  public function getData($handle = '', $hashtags = []) {
    if ($cache = $this->cache->get('social_network_feed:ig_' . $handle)) {
      $this->posts = $cache->data;
    }
    else {
      $this->getIGPosts($handle, $hashtags);
      // Checks global cache settings for social_network_feed.
      $cache = $this->config->get('cache_max_age');
      // Posts are cached as much time as set in the module's global configuration.
      if (isset($cache) && $cache != 'None') {
        $this->cache->set('social_network_feed:ig_' . $handle, $this->posts, strtotime('+' . $cache));
      }
    }

    foreach ($this->posts as $key => $item) {
      $ig_posts[] = [
        '#theme' => 'social_network_feed_instagram',
        '#post' => $item,
      ];
    }

    return [
      '#theme' => 'social_network_feed',
      '#content' => $ig_posts,
    ];
  }

  /**
   * Uses Instagram Basic Display API to retrieve posts and return them.
   *
   * @param $handle
   *   The account handle to retrieve posts from.
   *
   * @return array
   *   The array with retrieved posts.
   */
  protected function getIGPosts($handle = '', $hashtags = []) {
    $config = $this->config->get('ig_accounts');
    $fields = 'id,caption,media_type,media_url,thumbnail_url,permalink,owner,username,timestamp';

    if (!empty($config)) {
      if (!empty($handle)) {
        foreach ($config as $formData) {
          if ($handle == $formData['ig_handle']) {
            // If ig_post_number is not set, default to 5.
            $formData['ig_posts_number'] = !$formData['ig_posts_number'] == '' ? $formData['ig_posts_number'] : 5;
            // Increase limit to search for the hashtag.
            if (!empty($hashtags)) {
              $limit = 50;
            }
            else {
              // Add a buffer to the limit for empty posts.
              $limit = $formData['ig_posts_number'] + 5;
            }
            $access_token = $formData['ig_access_token'];
            $uri = strtr('https://graph.instagram.com/%ig_page_id/media', ['%ig_page_id' => $formData['ig_page_id']]);
            break;
          }
        }
      }
    }

    if (isset($limit, $access_token, $uri)) {
      try {
        $json_link = \Drupal::httpClient()->get($uri, [
          'query' => [
            'access_token' => $access_token,
            'fields' => $fields,
            'limit' => $limit,
          ]
        ]);
        $obj = Json::decode($json_link->getBody());
        $postData = $obj['data'];

        if (!empty($postData)) {
          foreach ($postData as $key => $item) {
            // Check for hashtag.
            if (!empty($hashtags)) {
              if (empty($item['caption'])) {
                unset($postData[$key]);
                continue;
              }

              $item['hashtag'] = FALSE;
              foreach ($hashtags as $hashtag) {
                $item['hashtag'] = social_network_feed_hashtag_filter($hashtag, $item['caption']);
                // If it has one of the hashtags it breaks out of the hashtag loop.
                if ($item['hashtag']) {
                  break;
                }
              }

              if (!$item['hashtag']) {
                unset($postData[$key]);
                continue;
              }
            }

            // Implements character limit and formats time.
            if (isset($item['caption']) && !empty($item['caption'])) {
              $postData[$key]['caption'] = Unicode::truncate($item['caption'], 128, FALSE, TRUE);
            }
            $postData[$key]['timestamp'] = date('d F Y', strtotime($item['timestamp']));
          }
          // Return limit back to the set number of posts.
          $postData = array_slice($postData, 0 , $formData['ig_posts_number']);

          return $this->posts = $postData;
        }
        else {
          return FALSE;
        }
      }
      catch (Exception $e) {
        watchdog_exception('social_network_feed.instagram', $e);
      }
    }

    return FALSE;
  }
}
