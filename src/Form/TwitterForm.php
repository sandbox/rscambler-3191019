<?php

namespace Drupal\social_network_feed\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure specific Twitter feed information.
 */
class TwitterForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_network_feed_tw_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['social_network_feed.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_network_feed.config');
    $config->clear('tw_settings');
    $form['#tree'] = TRUE;

    $form['tw_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Twitter settings'),
      '#prefix' => '<div id="tw-settings">',
      '#suffix' => '</div>',
      '#description' => $this->t('You must create a Twitter App to obtain all the information requested above.
        Click <a href="https://developer.twitter.com/en">here</a> to create a Twitter developer account and go to "Apps".'),
    ];

    $accounts = $config->get('tw_accounts');
    if (!isset($accounts) || empty($accounts)) {
      $accounts = [1];
    }

    $num_accounts = $form_state->get('num_accounts');
    // Ensure num_accounts is not empty.
    if ($num_accounts === NULL) {
      $num_accounts = count($accounts);
    }
    $form_state->set('num_accounts', $num_accounts);

    // Loop that adds a new form.
    for ($i = 0; $i < $num_accounts; $i++) {
      if ($handle[$i] = $accounts[$i]['tw_handle']) {
        $form['tw_settings']['title'][$i] = [
          '#type' => 'details',
          '#title' => $this->t($handle[$i]),
        ];
      }
      else {
        $form['tw_settings']['title'][$i] = [
          '#type' => 'details',
          '#title' => $this->t('New Account'),
        ];
      }

      $form['tw_settings']['title'][$i]['tw_handle'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Twitter Account Handle'),
        '#required' => TRUE,
        '#default_value' => $accounts[$i]['tw_handle'],
      ];

      $form['tw_settings']['title'][$i]['tw_user_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Twitter User ID'),
        '#required' => TRUE,
        '#default_value' => $accounts[$i]['tw_user_id'],
        '#description' => $this->t('User ID is required for correct authorization. Go to Settings&Privacy, <strong>
          Your Twitter data</strong> and then <strong>Account</strong> to find your User ID.'),
      ];

      $form['tw_settings']['title'][$i]['tw_api_key'] = [
        '#type' => 'textfield',
        '#title' => $this->t('API key'),
        '#default_value' => $accounts[$i]['tw_api_key'],
        '#required' => TRUE,
        '#description' => $this->t('Corresponds to consumer key. Needed for authorization.'),
      ];

      $form['tw_settings']['title'][$i]['tw_api_secret_key'] = [
        '#type' => 'textfield',
        '#title' => $this->t('API secret key'),
        '#default_value' => $accounts[$i]['tw_api_secret_key'],
        '#required' => TRUE,
        '#description' => $this->t('Corresponds to consumer secret key. Needed for authorization.'),
      ];

      $form['tw_settings']['title'][$i]['tw_access_token'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Access token'),
        '#default_value' => $accounts[$i]['tw_access_token'],
        '#required' => TRUE,
        '#description' => $this->t('Corresponds to OAuth token. Make sure that it\'s not invalid or expired to avoid
        possible issues.'),
      ];

      $form['tw_settings']['title'][$i]['tw_access_token_secret'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Access token secret'),
        '#default_value' => $accounts[$i]['tw_access_token_secret'],
        '#required' => TRUE,
        '#description' => $this->t('Corresponds to OAuth token secret. Make sure that it\'s not invalid or expired to
        avoid possible issues.'),
      ];

      $form['tw_settings']['title'][$i]['tw_posts_number'] = [
        '#type' => 'number',
        '#required' => TRUE,
        '#title' => $this->t('Number of posts to fetch.'),
        '#min' => 1,
        '#max' => 50,
        '#default_value' => $accounts[$i]['tw_posts_number'],
      ];

      $form['tw_settings']['title'][$i]['tw_character_limit'] = [
        '#type' => 'number',
        '#required' => FALSE,
        '#title' => $this->t('Character Limit'),
        '#default_value' => $accounts[$i]['tw_character_limit'],
      ];
    }

    $form['tw_settings']['actions'] = [
      '#type' => 'actions',
    ];

    $form['tw_settings']['actions']['add_account'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'tw-settings',
      ],
    ];

    // If there is more than one account, add the remove button.
    if ($num_accounts > 1) {
      $form['tw_settings']['actions']['remove_account'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'tw-settings',
        ],
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Selects and returns the fields.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['tw_settings'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_accounts');
    $add_button = $name_field + 1;
    $form_state->set('num_accounts', $add_button);

    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_accounts');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_accounts', $remove_button);
    }

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $accounts = $form_state->getValues()['tw_settings']['title'];

    $this->config('social_network_feed.config')
      ->set('tw_accounts', $accounts)
      ->save();
    parent::submitForm($form, $form_state);
  }
}
