<?php

namespace Drupal\social_network_feed\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure specific Facebook feed information.
 */
class InstagramForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_network_feed_ig_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['social_network_feed.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_network_feed.config');

    $form['#tree'] = TRUE;
    $form['ig_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Instagram settings'),
      '#prefix' => '<div id="ig-settings">',
      '#suffix' => '</div>',
    ];

    $accounts = $config->get('ig_accounts');
    if (!isset($accounts) || empty($accounts)) {
      $accounts = [1];
    }
    $num_accounts = $form_state->get('num_accounts');
    // Ensure num_accounts is not empty.
    if ($num_accounts === NULL) {
      $num_accounts = count($accounts);
    }
    $form_state->set('num_accounts', $num_accounts);

    // Loop that adds a new form.
    for ($i = 0; $i < $num_accounts; $i++) {

      if ($handle[$i] = $accounts[$i]['ig_handle']) {
        $form['ig_settings']['title'][$i] = [
          '#type' => 'details',
          '#title' => $this->t($handle[$i]),
        ];
      }
      else {
        $form['ig_settings']['title'][$i] = [
          '#type' => 'details',
          '#title' => $this->t('New Account'),
        ];
      }

      $form['ig_settings']['title'][$i]['ig_handle'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Instagram Account Handle'),
        '#required' => TRUE,
        '#default_value' => $accounts[$i]['ig_handle'],
        '#description' => $this->t('Your Instagram account must be linked to your Facebook page.'),
      ];

      $form['ig_settings']['title'][$i]['ig_access_token'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Instagram Access Token'),
        '#default_value' => $accounts[$i]['ig_access_token'],
        '#required' => TRUE,
        '#description' => $this->t('The access token is the same one used for your associated Facebook page.'),
        '#maxlength' => 512,
      ];

      $form['ig_settings']['title'][$i]['ig_page_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Instagram Page ID'),
        '#required' => TRUE,
        '#default_value' => $accounts[$i]['ig_page_id'],
        '#description' => $this->t('Steps 3-5 can help you get the Page ID for your account - <a href="https://developers.facebook.com/docs/instagram-api/getting-started/" target="_blank">Getting Started - Instagram API</a>'),
      ];

      $form['ig_settings']['title'][$i]['ig_posts_number'] = [
        '#type' => 'number',
        '#required' => FALSE,
        '#title' => $this->t('Number of posts'),
        '#min' => 1,
        '#max' => 50,
        '#default_value' => $accounts[$i]['ig_posts_number'],
      ];

      $form['ig_settings']['title'][$i]['ig_character_limit'] = [
        '#type' => 'number',
        '#required' => FALSE,
        '#title' => $this->t('Character Limit'),
        '#default_value' => $accounts[$i]['ig_character_limit'],
      ];
    }

    $form['ig_settings']['actions'] = [
      '#type' => 'actions',
    ];

    $form['ig_settings']['actions']['add_account'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'ig-settings',
      ],
    ];

    // If there is more than one account, add the remove button.
    if ($num_accounts > 1) {
      $form['ig_settings']['actions']['remove_account'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'ig-settings',
        ],
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Selects and returns the fields.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['ig_settings'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_accounts');
    $add_button = $name_field + 1;
    $form_state->set('num_accounts', $add_button);

    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_accounts');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_accounts', $remove_button);
    }

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $accounts = $form_state->getValues()['ig_settings']['title'];

    $this->config('social_network_feed.config')
      ->set('ig_accounts', $accounts)
      ->save();
    parent::submitForm($form, $form_state);
  }
}
