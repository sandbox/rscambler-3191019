<?php

namespace Drupal\social_network_feed\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure specific Facebook feed information.
 */
class FacebookForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_network_feed_fb_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['social_network_feed.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_network_feed.config');

    $form['#tree'] = TRUE;
    $form['fb_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Facebook settings'),
      '#prefix' => '<div id="fb-settings">',
      '#suffix' => '</div>',
    ];

    $accounts = $config->get('fb_accounts');
    if (!isset($accounts) || empty($accounts)) {
      $accounts = [1];
    }
    $num_accounts = $form_state->get('num_accounts');
    // Ensure num_accounts is not empty.
    if ($num_accounts === NULL) {
      $num_accounts = count($accounts);
    }
    $form_state->set('num_accounts', $num_accounts);

    // Loop that adds a new form.
    for ($i = 0; $i < $num_accounts; $i++) {

      if ($handle[$i] = $accounts[$i]['fb_handle']) {
        $form['fb_settings']['title'][$i] = [
          '#type' => 'details',
          '#title' => $this->t($handle[$i]),
        ];
      }
      else {
        $form['fb_settings']['title'][$i] = [
          '#type' => 'details',
          '#title' => $this->t('New Account'),
        ];
      }

      $form['fb_settings']['title'][$i]['fb_handle'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Facebook Account Handle'),
        '#required' => TRUE,
        '#default_value' => $accounts[$i]['fb_handle'],
      ];

      $form['fb_settings']['title'][$i]['fb_app_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Facebook App ID'),
        '#default_value' => $accounts[$i]['fb_app_id'],
        '#required' => TRUE,
        '#description' => $this->t('You must register a Facebook app to use this module. You can register a app by
      <a href="https://developers.facebook.com/apps/" target="_blank">clicking here</a>.'),
      ];

      $form['fb_settings']['title'][$i]['fb_app_secret'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Facebook App Secret'),
        '#default_value' => $accounts[$i]['fb_app_secret'],
        '#required' => TRUE,
        '#description' => $this->t('The app secret can be found after creating a Facebook app in the API console.'),
      ];

      $form['fb_settings']['title'][$i]['fb_access_token'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Facebook Access Token'),
        '#default_value' => $accounts[$i]['fb_access_token'],
        '#required' => TRUE,
        '#description' => $this->t('The access token can be found after creating a Facebook app in the API console.'),
        '#maxlength' => 512,
      ];

      $form['fb_settings']['title'][$i]['fb_page_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Facebook Page ID'),
        '#required' => TRUE,
        '#default_value' => $accounts[$i]['fb_page_id'],
      ];

      $form['fb_settings']['title'][$i]['fb_posts_number'] = [
        '#type' => 'number',
        '#required' => FALSE,
        '#title' => $this->t('Number of posts'),
        '#min' => 1,
        '#max' => 50,
        '#default_value' => $accounts[$i]['fb_posts_number'],
      ];

      $form['fb_settings']['title'][$i]['fb_character_limit'] = [
        '#type' => 'number',
        '#required' => FALSE,
        '#title' => $this->t('Character Limit'),
        '#default_value' => $accounts[$i]['fb_character_limit'],
      ];
    }

    $form['fb_settings']['actions'] = [
      '#type' => 'actions',
    ];

    $form['fb_settings']['actions']['add_account'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'fb-settings',
      ],
    ];

    // If there is more than one account, add the remove button.
    if ($num_accounts > 1) {
      $form['fb_settings']['actions']['remove_account'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'fb-settings',
        ],
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Selects and returns the fields.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['fb_settings'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_accounts');
    $add_button = $name_field + 1;
    $form_state->set('num_accounts', $add_button);

    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_accounts');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_accounts', $remove_button);
    }

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $accounts = $form_state->getValues()['fb_settings']['title'];

    $this->config('social_network_feed.config')
      ->set('fb_accounts', $accounts)
      ->save();
    parent::submitForm($form, $form_state);
  }
}
