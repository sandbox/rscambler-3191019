<?php

namespace Drupal\social_network_feed\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Global configuration for all social media feeds.
 */
class GlobalForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_network_feed_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['social_network_feed.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_network_feed.config');

    $form['global_configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Global configuration'),
    ];

    $form['global_configuration']['items_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of items per page'),
      '#min' => 1,
      '#max' => 25,
      '#default_value' => $config->get('items_per_page'),
    ];

    $options = [
      'None' => 'None',
      '5 mins' => '5 mins',
      '10 mins' => '10 mins',
      '15 mins' => '15 mins',
      '30 mins' => '30 mins',
      '1 hour' => '1 hour',
      '3 hours' => '3 hours',
      '6 hours' => '6 hours',
      '1 day' => '1 day',
    ];
    $form['global_configuration']['cache_max_age'] = [
      '#type' => 'select',
      '#title' => $this->t('Cache maximum age'),
      '#options' => $options,
      '#default_value' => array($config->get('cache_max_age')),
      '#multiple' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('social_network_feed.config')
        // Set the submitted configuration setting
        ->set('items_per_page', $form_state->getValue('items_per_page'))
        ->set('cache_max_age', $form_state->getValue('cache_max_age'))
        ->save();

    parent::submitForm($form, $form_state);
  }

}
