<?php

/**
 * @file
 * Contains \Drupal\social_network_feed\Form\SocialFeedComponentYoutube.
 */

namespace Drupal\social_network_feed\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure specific Youtube feed information.
 */
class YoutubeForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_network_feed_yt_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['social_network_feed.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_network_feed.config');
    $form['#tree'] = TRUE;

    $form['yt_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Youtube settings'),
      '#prefix' => '<div id="yt-settings">',
      '#suffix' => '</div>',
    ];

    $accounts = $config->get('yt_accounts');
    if (!isset($accounts) || empty($accounts)) {
      $accounts = [1];
    }

    $num_accounts = $form_state->get('num_accounts');
    // Ensure num_accounts is not empty.
    if ($num_accounts === NULL) {
      $num_accounts = count($accounts);
    }
    $form_state->set('num_accounts', $num_accounts);

    // Loop that adds a new form.
    for ($i = 0; $i < $num_accounts; $i++) {
      if ($handle[$i] = $accounts[$i]['yt_handle']) {
        $form['yt_settings']['title'][$i] = [
          '#type' => 'details',
          '#title' => $this->t($handle[$i]),
        ];
      }
      else {
        $form['yt_settings']['title'][$i] = [
          '#type' => 'details',
          '#title' => $this->t('New Account'),
        ];
      }

      $form['yt_settings']['title'][$i]['yt_handle'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Youtube account handle'),
        '#required' => TRUE,
        '#default_value' => $accounts[$i]['yt_handle'],
      ];

      $form['yt_settings']['title'][$i]['yt_api_key'] = [
        '#type' => 'textfield',
        '#title' => $this->t('YouTube API key'),
        '#required' => TRUE,
        '#default_value' => $accounts[$i]['yt_api_key'],
        '#description' => $this->t('Get your API key from <a href="https://console.developers.google.com/apis/credentials">
        Google developers console</a>'),
      ];

      $form['yt_settings']['title'][$i]['yt_channel_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('YouTube channel ID'),
        '#required' => TRUE,
        '#default_value' => $accounts[$i]['yt_channel_id'],
        '#description' => $this->t('The channel ID from where you want to fetch the videos.'),
      ];

      $form['yt_settings']['title'][$i]['yt_posts_number'] = [
        '#type' => 'number',
        '#required' => TRUE,
        '#title' => $this->t('Number of videos to fetch'),
        '#min' => 1,
        '#max' => 50,
        '#default_value' => $accounts[$i]['yt_posts_number'],
      ];
    }

    $form['yt_settings']['actions'] = [
      '#type' => 'actions',
    ];

    $form['yt_settings']['actions']['add_account'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'yt-settings',
      ],
    ];

    // If there is more than one account, add the remove button.
    if ($num_accounts > 1) {
      $form['yt_settings']['actions']['remove_account'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'yt-settings',
        ],
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Selects and returns the fields.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['yt_settings'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_accounts');
    $add_button = $name_field + 1;
    $form_state->set('num_accounts', $add_button);

    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_accounts');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_accounts', $remove_button);
    }

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $accounts = $form_state->getValues()['yt_settings']['title'];

    $this->config('social_network_feed.config')
      ->set('yt_accounts', $accounts)
      ->save();

    parent::submitForm($form, $form_state);
  }
}
