<?php

/**
 * @file
 * Contains \Drupal\social_network_feed\FacebookFeed.
 */

namespace Drupal\social_network_feed;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Uses Faceboook API to retrieve profile posts.
 */
class FacebookFeed {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Cache backend instance to use.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The posts to be retrieved and returned by the service.
   *
   * @var array
   */
  protected $posts = [];

  /**
   * Constructs a new FacebookFeed object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(ConfigFactoryInterface $config, CacheBackendInterface $cache) {
    $this->config = $config->getEditable('social_network_feed.config');
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.config')
    );
  }

  /**
   * Checks the feed in cache and if not calls getFBPosts().
   *
   * @param $handle
   *   The account handle to retrieve posts from.
   *
   * @return array
   *   The renderable array with retrieved posts.
   */
  public function getData($handle = '', $hashtags = []) {
    if ($cache = $this->cache->get('social_network_feed:fb_' . $handle)) {
      $this->posts = $cache->data;
    }
    else {
      $this->getFBPosts($handle, $hashtags);
      // Checks global cache settings for social_network_feed.
      $cache = $this->config->get('cache_max_age');

      if (isset($cache) && $cache != 'None') {
        $this->cache->set('social_feed_component:fb_' . $handle, $this->posts, strtotime('+' . $cache));
      }
    }

    $profile_pic = NULL;
    if (isset($this->posts['profile_pic'])) {
      $profile_pic = $this->posts['profile_pic'];
      // Remove before foreach function below.
      unset($this->posts['profile_pic']);
    }
    foreach ($this->posts as $key => $item) {
      $fb_posts[] = [
        '#theme' => 'social_network_feed_facebook',
        '#post' => $item,
        '#profile_pic' => $profile_pic,
      ];
    }

    return [
      '#theme' => 'social_network_feed',
      '#content' => $fb_posts,
    ];
  }

  /**
   * Uses Facebook API to retrieve posts and return them.
   *
   * @param $handle
   *   The account handle to retrieve posts from.
   *
   * @return array
   *   The array with retrieved posts.
   */
  protected function getFBPosts($handle = '', $hashtags = []) {
    $config = $this->config->get('fb_accounts');
    $fields = 'id,message,picture,attachments,icon,created_time,from';

    if (!empty($config)) {
      if (!empty($handle)) {
        foreach ($config as $formData) {
          if ($handle == $formData['fb_handle']) {
            // If fb_post_number is not set, default to 5.
            $formData['fb_posts_number'] = !$formData['fb_posts_number'] == '' ? $formData['fb_posts_number'] : 5;
            // Increase limit to search for the hashtag.
            if (!empty($hashtags)) {
              $limit = 50;
            }
            else {
              // Add a buffer to the limit for empty posts.
              $limit = $formData['fb_posts_number'] + 5;
            }
            $access_token = $formData['fb_access_token'];
            $uri = strtr('https://graph.facebook.com/%fb_page_id/feed', ['%fb_page_id' => $formData['fb_page_id']]);
            break;
          }
        }
      }
    }

    if (isset($access_token, $limit, $uri)) {
      try {
        $json_link = \Drupal::httpClient()->get($uri, [
          'query' => [
            'access_token' => $access_token,
            'fields' => $fields,
            'limit' => $limit,
          ]
        ]);
        $obj = Json::decode($json_link->getBody());
        $postData = $obj['data'];

        if (!empty($postData)) {
          // Implements character limit and formats time.
          foreach ($postData as $key => $item) {
            // Removes empty statuses.
            if (!isset($item['attachments']) && !isset($item['message'])) {
              unset($postData[$key]);
              continue;
            }

            if (isset($item['attachments']['data'][0])) {
              $attachments = $item['attachments']['data'][0];
              $item['description'] = isset($attachments['description']) ? $attachments['description'] : FALSE;
              $item['name'] = isset($attachments['title']) ? $attachments['title'] : FALSE;
              $item['link'] = isset($attachments['url']) ? $attachments['url'] : FALSE;
              $item['type'] = isset($attachments['type']) ? $attachments['type'] : FALSE;
            }

            // Check for hashtag.
            if (!empty($hashtags)) {
              $item['hashtag'] = FALSE;
              $text = !empty($item['message']) ? $item['message'] : $item['description'];
              if (!empty($text)) {
                foreach ($hashtags as $hashtag) {
                  $item['hashtag'] = social_network_feed_hashtag_filter($hashtag, $text);
                  // If it has any hashtag it breaks out of the hashtag loop.
                  if ($item['hashtag']) {
                    break;
                  }
                }
              }

              if (!$item['hashtag']) {
                unset($postData[$key]);
                continue;
              }
            }

            if ((isset($formData['fb_character_limit']) && !empty($formData['fb_character_limit'])) || isset($item['picture'])) {
              if (isset($item['picture'])) {
                $char_limit = 128;
              }
              else {
                $char_limit = $formData['fb_character_limit'];
              }

              $item['message'] = isset($item['message']) ? Unicode::truncate($item['message'], $char_limit, FALSE, TRUE) : FALSE;
              $item['description'] = isset($item['description']) ? Unicode::truncate($item['description'], $char_limit, FALSE, TRUE) : FALSE;
            }

            $item['created_time'] = date('d F Y', strtotime($item['created_time']));

            $postData[$key] = $item;
          }

          // Return limit back to the set number of posts.
          $postData = array_slice($postData, 0, $formData['fb_posts_number']);

          // Get Profile Picture.
          $uri = strtr('https://graph.facebook.com/%fb_page_id/picture', ['%fb_page_id' => $formData['fb_page_id']]);

          $profile_pic = \Drupal::httpClient()->get($uri, [
            'query' => [
              'redirect' => 0,
              'access_token' => $access_token,
            ]
          ]);
          $obj = Json::decode($profile_pic->getBody());
          $profile_pic = isset($obj['data']['url']) ? $obj['data']['url'] : NULL;

          $postData['profile_pic'] = $profile_pic;

          return $this->posts = $postData;
        }
        else {
          return FALSE;
        }
      }
      catch (Exception $e) {
        watchdog_exception('social_network_feed.facebook', $e);
      }
    }

    return FALSE;
  }
}
