<?php

/**
 * @file
 * Contains \Drupal\social_network_feed\TwitterFeed.
 */

namespace Drupal\social_network_feed;

use Abraham\TwitterOAuth\TwitterOAuth;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Uses Twitter API to retrieve user timelines.
 */
class TwitterFeed {

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\Config $config
   */
  protected $config;

  /**
   * Cache backend instance to use.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Logger channel to set a message for reponse errors.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The tweets to be retrieved and returned by the service.
   *
   * @var array
   */
  protected $tweets = [];

  /**
   * Constructs a new TwitterFeed object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(ConfigFactoryInterface $config, CacheBackendInterface $cache_backend,
                              DateFormatterInterface $date_formatter, LoggerChannelFactoryInterface $logger_factory) {
    $this->config = $config->getEditable('social_network_feed.config');
    $this->cache = $cache_backend;
    $this->dateFormatter = $date_formatter;
    $this->logger = $logger_factory->get('social_network_feed.twitter');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.config'),
      $container->get('date.formatter'),
      $container->get('logger.factory')
    );
  }

  /**
   * Checks the feed in cache and if not it calls getTweets().
   *
   * @param $handle
   *   The account handle to retrieve tweets from.
   *
   * @return array
   *   The Twitter feed renderable array.
   */
  public function getData($handle = '', $hashtags = []) {
    if ($module_cache = $this->cache->get('social_network_feed:twitter')) {
      $this->tweets = $module_cache->data;
    }
    else {
      $this->getTweets($handle, $hashtags);
      $module_cache = $this->config->get('cache_max_age');
      // Tweets are cached as much time as set in the module's global configuration.
      if (isset($module_cache) && $module_cache != 'None') {
        $this->cache->set('social_network_feed:twitter', $this->tweets, strtotime('+' . $module_cache));
      }
    }

    return [
      '#theme' => 'social_network_feed',
      '#content' => $this->tweets,
    ];
  }

  /**
   * Uses Twitter API to retrieve tweets and store them in $this->tweets as renderable arrays.
   *
   * @param $handle
   *   The account handle to retrieve tweets from.
   */
  private function getTweets($handle = '', $hashtags = []) {
    $accounts = $this->config->get('tw_accounts');
    // If there's any account set in the module's config, pull the information from the handle supplied. Set accounts in
    // /admin/config/services/social-feeds/config/twitter.
    if (!empty($accounts)) {
      if (!empty($handle)) {
        foreach ($accounts as $twitter_account) {
          if ($handle == $twitter_account['tw_handle']) {
            $api_key = $twitter_account['tw_api_key'];
            $secret_key = $twitter_account['tw_api_secret_key'];
            $access_token = $twitter_account['tw_access_token'];
            $access_token_secret = $twitter_account['tw_access_token_secret'];
            if (empty($hashtags)) {
              $posts_number = $twitter_account['tw_posts_number'];
            }
            else {
              $posts_number = 50;
            }
            $user_id = $twitter_account['tw_user_id'];
            break;
          }
        }
      }
    }
    // If there's any information pulled, do the API calls.
    if (isset($api_key, $secret_key, $access_token, $access_token_secret)) {
      try {
        // Use TwitterOAuth library to get the bearer token for authorization.
        $connection = new TwitterOAuth($api_key, $secret_key, $access_token, $access_token_secret);
        $parameters = [
          'user_id' => $user_id,
          'count' => $posts_number,
          'exclude_replies' => TRUE,
          'tweet_mode' => 'extended',
        ];
        // Retrieve the tweets from the current handle's user timeline.
        $response = $connection->get('/statuses/user_timeline', $parameters);
        // Process data to be renderable if no errors are triggered.
        if (!isset($response->errors)) {

          foreach ($response as $key => $tweet_response) {

            // Check for hashtag.
            if (!empty($hashtags)) {
              $tweet_response->hashtag = FALSE;
              foreach ($hashtags as $hashtag) {
                $tweet_response->hashtag = social_network_feed_hashtag_filter($hashtag, $tweet_response->full_text);
                // If it has one of the hashtags it breaks out of the hashtag loop.
                if ($tweet_response->hashtag) {
                  break;
                }
              }

              if (!$tweet_response->hashtag) {
                unset($response[$key]);
                continue;
              }
            }

            $tweet['date'] = $this->dateFormatter->format(strtotime($tweet_response->created_at), 'short');
            $tweet['username'] = $tweet_response->user->name;
            $tweet['screen_name'] = $tweet_response->user->screen_name;
            $tweet['message'] = $tweet_response->full_text;
            $tweet['profile_image'] = $tweet_response->user->profile_image_url_https;
            $tweet['link'] = 'https://twitter.com/' . $tweet_response->user->screen_name . '/status/' . $tweet_response->id_str;
            $tweet['media_url'] = NULL;
            $tweet['media_type'] = NULL;

            if (isset($tweet_response->extended_entities->media[0])) {
              $tweet['media_url'] = $tweet_response->extended_entities->media[0]->media_url_https;
              $tweet['media_type'] = 'photo';

              switch ($tweet_response->extended_entities->media[0]->type) {
                case 'animated_gif':
                  if (isset($tweet_response->extended_entities->media[0]->video_info->variants[0])) {
                    $tweet['media_type'] = 'animated_gif';
                  }
                  break;

                case 'video':
                  if (isset($tweet_response->extended_entities->media[0]->video_info->variants[1])) {
                    $tweet['media_type'] = 'video';
                  } else if (isset($tweet_response->extended_entities->media[0]->video_info->variants[0])) {
                    $tweet['media_type'] = 'video';
                  }
                  break;
              }
            }

            $this->tweets[] = [
              '#theme' => 'social_network_feed_twitter',
              '#tweet' => $tweet,
            ];
          }
          // Return limit back to the set number of posts.
          $this->tweets = array_slice($this->tweets, 0, $twitter_account['tw_posts_number']);
        }
        else {
          $message = 'Code ' . $response->errors[0]->code . ': ' . $response->errors[0]->message;
          $this->logger->error($message);
        }
      }
      catch (Exception $e) {
        watchdog_exception('social_network_feed.twitter', $e);
      }
    }
  }
}
