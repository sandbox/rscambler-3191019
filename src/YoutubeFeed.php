<?php

/**
 * @file
 * Contains \Drupal\social_network_feed\YoutubeFeed.
 */

namespace Drupal\social_network_feed;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;

/**
 * Uses Youtube API to retrieve channel posts.
 */
class YoutubeFeed {

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\Config $config
   */
  protected $config;

  /**
   * Cache backend instance to use.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * An http client.
   *
   * @var \GuzzleHttp\Client $httpClient
   */
  protected $httpClient;

  /**
   * The videos to be retrieved and returned by the service.
   *
   * @var array
   */
  protected $videos = [];

  /**
   * Constructs a new YoutubeFeed object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   An HTTP client.
   */
  public function __construct(ConfigFactoryInterface $config, CacheBackendInterface $cache_backend, DateFormatterInterface $date_formatter, ClientInterface $http_client) {
    $this->config = $config->getEditable('social_network_feed.config');
    $this->cache = $cache_backend;
    $this->dateFormatter = $date_formatter;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.config'),
      $container->get('date.formatter'),
      $container->get('http_client')
    );
  }

  /**
   * Checks the feed in cache and if not calls getVideos().
   *
   * @param $handle
   *   The account handle to retrieve videos from.
   *
   * @return array
   *   The renderable array with retrieved videos.
   */
  public function getData($handle = '', $hashtags = []) {
    if ($module_cache = $this->cache->get('social_network_feed:yt_' . $handle)) {
      $this->videos = $module_cache->data;
    }
    else {
      $this->getVideos($handle, $hashtags);
      // Checks global cache settings for social_network_feed.
      $module_cache = $this->config->get('cache_max_age');
      // Videos are cached as much time as set in the module's global configuration.
      if (isset($module_cache) && $module_cache != 'None') {
        $this->cache->set('social_network_feed:yt_' . $handle, $this->videos, strtotime('+' . $module_cache));
      }
    }

    return [
      '#theme' => 'social_network_feed',
      '#content' => $this->videos,
    ];
  }

  /**
   * Uses Youtube API to retrieve videos and store them in $this->videos as renderable arrays.
   *
   * @param $handle
   *   The account handle to retrieve videos from.
   */
  private function getVideos($handle = '', $hashtags = []) {
    $endpoint_url = 'https://www.googleapis.com/youtube/v3/search';
    // Fill your account feeds in '/admin/config/services/social-feeds/config/youtube'.
    $accounts = $this->config->get('yt_accounts');
    $body = [
      'query' => [
        'part' => 'snippet',
        'order' => 'date',
        'type' => 'video',
      ],
    ];

    if (!empty($accounts)) {
      if (!empty($handle)) {
        foreach ($accounts as $youtube_account) {
          if ($handle == $youtube_account['yt_handle']) {
            $body['query']['key'] = $youtube_account['yt_api_key'];
            $body['query']['channelId'] = $youtube_account['yt_channel_id'];
            $body['query']['maxResults'] = $youtube_account['yt_posts_number'];
            break;
          }
        }
      }
    }
    // If there's any information pulled, do the API calls.
    if (isset($body['query']['key'], $body['query']['channelId'], $body['query']['maxResults'])) {
      try {
        $response = $this->httpClient->get($endpoint_url, $body);
        // If response is OK, decode JSON response and store renderable arrays in $posts.
        if ($response->getStatusCode() == 200) {
          $results = Json::decode($response->getBody()->getContents());

          foreach ($results['items'] as $key => $item) {
            $item['snippet']['title'] = Html::decodeEntities($item['snippet']['title']);
            $item['snippet']['description'] = Html::decodeEntities($item['snippet']['description']);
            // Check for hashtag.
            if (!empty($hashtags)) {
              $item['hashtag'] = FALSE;
              foreach ($hashtags as $hashtag) {
                $item['hashtag'] = social_network_feed_hashtag_filter($hashtag, $item['snippet']['description']);
                // If it has one of the hashtags it breaks out of the hashtag loop.
                if ($item['hashtag']) {
                  break;
                }
              }

              if (!$item['hashtag']) {
                unset($results['items'][$key]);
                continue;
              }
            }

            $video = [
              'video_id' => $item['id']['videoId'],
              'title' => $item['snippet']['title'],
              'description' => $item['snippet']['description'],
              'channel_id' => $item['snippet']['channelId'],
              'channel_title' => $item['snippet']['channelTitle'],
              // Time stored in unix epoch format.
              'published_date' => $this->dateFormatter->format(strtotime($item['snippet']['publishedAt'])),
              'thumbnail' => [
                '#theme' => 'image',
                '#uri' => $item['snippet']['thumbnails']['default']['url'],
              ],
              'medium' => [
                '#theme' => 'image',
                '#uri' => $item['snippet']['thumbnails']['medium']['url'],
              ],
              'high' => [
                '#theme' => 'image',
                '#uri' => $item['snippet']['thumbnails']['high']['url'],
              ],
            ];

            $this->videos[] = [
              '#theme' => 'social_network_feed_youtube',
              '#video' => $video,
            ];
          }
        }
      }
      catch (Exception $e) {
        watchdog_exception('social_network_feed.youtube', $e);
      }
    }
  }

}
